import * as d3 from 'd3';

// Remove old g. Start with blank svg
const removeAllG = (svgRef) => d3
  .select(svgRef)
  .select('g')
  .remove();

// Create data for Radial Tidy Tree
const rootData = (data, size = 800) => d3
  .tree()
  .size([2 * Math.PI, size / 2])
  .separation((a, b) => (a.parent === b.parent ? 1 : 6) / a.depth)(d3.hierarchy(data));

// Draw Mindmap svg g element
const createMindMap = (data, div, size, textSize = 200, {
  divClass = 'note', linkClass = 'link', nodeClass = 'node', nodeInternalClass = 'node--internal', nodeLeafClass = 'node--leaf',
} = {}) => {
  // Prepare data
  const root = rootData(data, size);
  // Create g and link paths
  const g = d3.create('svg:g');
  g.append('g')
    .selectAll(linkClass)
    .data(root.links())
    .enter()
    .append('path')
    .attr('class', linkClass)
    .attr('fill', 'none')
    .attr('stroke', (d) => d.target.data.color)
    .attr('d', d3
      .linkRadial()
      .angle((d) => d.x)
      .radius((d) => d.y));
  // Append Nodes
  const node = g
    .append('g')
    .selectAll(nodeClass)
    .data(root.descendants())
    .enter()
    .append('g')
    .attr('class', (d) => `${nodeClass}${d.children ? ` ${nodeInternalClass}` : ` ${nodeLeafClass}`}`)
    .attr('transform', (d) => `rotate(${(d.x * 180) / Math.PI - 90}) translate(${d.y},0)`);
  // Append Texts
  node
    .append('text')
    .style('font-size', `${textSize}px`)
    .attr('dy', '0.31em')
    .attr('x', (d) => {
      const pos = d.x < Math.PI;
      return pos === !d.children ? 5 : -5;
    })
    .attr('text-anchor', (d) => {
      const pos = d.x < Math.PI;
      return pos === !d.children ? 'start' : 'end';
    })
    .attr('transform', (d) => (d.x >= Math.PI ? 'rotate(180)' : null))
    .text((d) => d.data.name);
  // If given selection div
  if (div) {
    div.attr('class', divClass).style('display', 'none');
    node
      .style('cursor', (d) => (d.data.note ? 'pointer' : 'move'))
      .style('text-decoration', (d) => (d.data.note ? 'underline' : 'none'))
      .on('mouseover', (d) => {
        if (d.data.note) {
          div.style('display', 'block').html(`<h1>${d.data.name}</h1>${d.data.note}`);
        }
      })
      .on('mouseout', () => {
        div.style('display', 'none').html('');
      });
  }
  return g.node();
};

// Calculate autobox with margins
const autoBox = (margin = 20) => {
  const adjust = margin / 2; // px
  // eslint-disable-next-line func-names
  return function () {
    const {
      x, y, width, height,
    } = this.getBBox();
    return [x - adjust, y - adjust, width + margin, height + margin];
  };
};

const mindMapD3 = (data, svgRef, tooltipRef, size, margin, textSize, styleConfig) => {
  // If object data with nodes availabe and svg selected
  if (data && svgRef) {
    // Remove old g
    removeAllG(svgRef);
    const div = d3.select(tooltipRef);
    // Draw new svg
    const svg = d3
      .select(svgRef)
      .call(
        d3.zoom().on('zoom', () => {
          svg.attr('transform', d3.event.transform);
        }),
      )
      .append(() => createMindMap(data, div, size, textSize, styleConfig));
    // Get newly created SVG viewBox sizes
    const autoBoxMargin = autoBox(margin);
    d3.select(svgRef).attr('viewBox', autoBoxMargin);
  }
};

export { mindMapD3, createMindMap, autoBox };
