/* eslint-disable jsx-a11y/label-has-associated-control */
import React from 'react';
import { Link } from 'react-router-dom';

const BurgerMenu = () => (
  <div className="burger">
    <input type="checkbox" className="burger__checkbox" id="nav-toggle" />
    <label htmlFor="nav-toggle" className="burger__button">
      <span className="burger__icon">&nbsp;</span>
    </label>
    <nav className="burger__menu">
      <Link to="/">Home</Link>
      <Link to="/config">Configuration</Link>
      <Link to="/help">Help</Link>
    </nav>
  </div>
);

export default BurgerMenu;
