/* eslint-disable react/prop-types */
import React from 'react';
import * as d3 from 'd3';
import { createMindMap, autoBox } from '../../../lib/mindMapD3';
// TODO
const DownloadSvg = ({
  getName, getData, getSize, getMargin, getTextSize,
}) => {
  const css = `
  .node text {
    font-family: 'Arial', sans-serif;
  }
   .node--internal text {
    paint-order: stroke;
    stroke: white;
    stroke-width: 1em;
  }
  .link {
    stroke-width: 1px;
  }`;

  function svgDownload(filename, data) {
    const svg = d3.select('body').append('svg');
    svg.append('style').text(css);
    svg.append(() => createMindMap(data, null, getSize, getTextSize));
    const autoBoxMargin = autoBox(getMargin);
    svg.attr('viewBox', autoBoxMargin);

    const serializer = new XMLSerializer();
    const source = serializer.serializeToString(svg.node());
    svg.remove();

    const element = document.createElement('a');
    element.setAttribute('href', `data:image/svg+xml;charset=utf-8,${encodeURIComponent(source)}`);
    element.setAttribute('download', filename);
    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
  }

  const downloadHandler = () => {
    svgDownload(`${getName}.svg`, getData);
  };

  return (
    <div>
      <button className="about__btn" type="button" onClick={downloadHandler}>
        Download SVG
      </button>
    </div>
  );
};

export default DownloadSvg;
