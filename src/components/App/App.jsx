import React, { useState, useEffect } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import '../../sass/main.scss';
import text2object from '../../lib/text2object';
import MindMapText from '../MindMapText/MindMapText';
import MindMap from '../MindMap/MindMap';
import ConfigPage from '../ConfigPage/ConfigPage';
import HelpPage from '../HelpPage/HelpPage';
import BurgerMenu from '../BurgerMenu/BurgerMenu';
import { initialName, initialText } from './initialValues';

const App = () => {
  const [getName, setName] = useState(initialName);
  const [getText, setText] = useState(initialText);
  const [getData, setData] = useState(null);
  const [getSize, setSize] = useState(400);
  const [getMargin, setMargin] = useState(50);
  const [getTextSize, setTextSize] = useState(13);
  const [getColor, setColor] = useState();

  useEffect(() => {
    setData(text2object(getText, getName, getColor));
  }, [getText, getName, getColor]);

  return (
    <Router>
      <header className="mm__header">
        <BurgerMenu />
      </header>
      <main className="mm__main">
        <Route
          exact
          path="/config"
          render={() => (
            <ConfigPage
              getName={getName}
              getText={getText}
              getData={getData}
              getSize={getSize}
              setSize={setSize}
              getMargin={getMargin}
              setMargin={setMargin}
              getTextSize={getTextSize}
              setTextSize={setTextSize}
              getColor={getColor}
              setColor={setColor}
            />
          )}
        />
        <Route exact path="/help" render={() => <HelpPage />} />
        <MindMapText getName={getName} setName={setName} getText={getText} setText={setText} />
        <MindMap data={getData} getSize={getSize} getMargin={getMargin} getTextSize={getTextSize} />
      </main>
    </Router>
  );
};

export default App;
