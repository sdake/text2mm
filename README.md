# text2mm.com

The text2mm.com is Mindmap creation from tab indented text application.

This repostiory depends on Node version 16.

## Install

Run in terminal

```
yarn start
```

## Test

Run in terminal

```
yarn test
```

## build

Run in terminal

```
yarn build
```
